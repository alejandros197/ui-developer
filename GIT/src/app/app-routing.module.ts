import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactosComponent } from './components/routes/contactos/contactos.component';
import { GaleriaComponent } from './components/routes/galeria/galeria.component';
import { InicioComponent } from './components/routes/inicio/inicio.component';


const routes: Routes = [
  {
    path:'inicio',
    component:InicioComponent,
    pathMatch: 'full'

  },
  {
    path:'galeria',
    component:GaleriaComponent
  },

  {
    path:'contactos',
    component:ContactosComponent
  },
  {
    path:'**',
    redirectTo: 'inicio'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
